package atualizadordeprecossispatri;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SqlServer 
{
    private ResultSet resultSetConsulta;
    private java.sql.Statement statement;
    private java.sql.Statement statementUpdate;
    private Connection connection;
    
    public SqlServer() 
    {
       
    }
 
    public ResultSet getResultSetConsulta()
    {
        return resultSetConsulta;
    }
    
    public java.sql.Statement getStatement()
    {
        return statement;
    }
    
    public java.sql.Statement getStatementUpdate()
    {
        return statementUpdate;
    }
    
    public boolean connect() throws InstantiationException, IllegalAccessException
    {
        boolean isConnected = false;
        try 
        {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
//            this.connection = DriverManager.getConnection("jdbc:sqlserver://172.31.0.45:1433;databaseName=Almox","almox", "almox"); // desenvolvimento
            this.connection = DriverManager.getConnection("jdbc:sqlserver://172.31.0.33:1433;databaseName=Almox","almox", "almox"); // produção
            statement = connection.createStatement();
            statementUpdate = connection.createStatement();
            isConnected = true;
        } 
        catch (ClassNotFoundException | SQLException e) 
        {
        }
        return isConnected;
    }
    
    public boolean disconnect()
    {
        boolean isConnected = false;
        try 
        {
           connection.close();
           statement.close();
           statementUpdate.close();
           resultSetConsulta.close();
           return isConnected = false;
        } 
        catch (SQLException e) 
        {
        }
        return isConnected = true;
    }
    
    public boolean execute(String query) throws SQLException
    {
        try
        {
            resultSetConsulta = statement.executeQuery(query);
            return true;
        }
        catch(SQLException e) 
        {
            return false;
        }
    }    
    
    public boolean executeUpdate(String query) throws SQLException
    {
        try
        {
            statementUpdate.executeUpdate(query);
            System.out.println("update executed");
            return true;
        }
        catch(SQLException e) 
        {
            System.out.println(e);
            return false;
        }
    }    
}
