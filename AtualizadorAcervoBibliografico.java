/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atualizadordeprecossispatri;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;

/**
 *
 * @author arthurpcb
 */
public class AtualizadorAcervoBibliografico 
{
    public static void main(String[] args) throws SQLException, InstantiationException, IllegalAccessException, FileNotFoundException, IOException
    {
        SqlServer banco = new SqlServer();
        
        if(banco.connect())
        {            
            FileReader arquivo = new FileReader("C:\\Users\\ArthurPCB.TCE-TO\\Desktop\\codigosPrecosBiblioteca.txt");
            BufferedReader lerArq = new BufferedReader(arquivo);
            String linha = lerArq.readLine();
            
            while(linha != null)
            {
                String linhaDividida[] = linha.split("\\s");
                String patrimonio = linhaDividida[0];
                String preco_atu = linhaDividida[1];
                String preco_atu_mod1 = preco_atu.replace(".", "");
                String preco_atu_mod2 = preco_atu_mod1.replace(",", ".");
                System.out.println("|----------------------------------------------|");
                System.out.println("|- Inseridno dados da linha - " + linha);
                System.out.println("|- Valores:");
                System.out.println("|- Código Livro - "+patrimonio);
                System.out.println("|- Preço Livro - "+preco_atu_mod2);

                banco.executeUpdate("insert into AcervoBibliograficoTemp (codigo, preco) values ("+ patrimonio +","+preco_atu_mod2+");");

                System.out.println("|----------------------------------------------|");
                System.out.println("");
                
                linha = lerArq.readLine();
            }
        }
    }
}
