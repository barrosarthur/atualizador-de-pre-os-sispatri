package atualizadordeprecossispatri;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;

public class AtualizadorDePrecosSispatri 
{

    public static void main(String[] args) throws SQLException, InstantiationException, IllegalAccessException, FileNotFoundException, IOException
    {
        SqlServer banco = new SqlServer();
        
        if(banco.connect())
        {            
            FileReader arquivo = new FileReader("C:\\Users\\ArthurPCB.TCE-TO\\Desktop\\precoAtu.txt");
            BufferedReader lerArq = new BufferedReader(arquivo);
            String linha = lerArq.readLine();
            
            while(linha != null)
            {
                String linhaDividida[] = linha.split("\\s");
                String patrimonio = linhaDividida[0];
                String preco_atu = linhaDividida[1];
                String preco_atu_mod1 = preco_atu.replace(".", "");
                String preco_atu_mod2 = preco_atu_mod1.replace(",", ".");
                System.out.println("|----------------------------------------------|");
                System.out.println("|- Inseridno dados da linha - " + linha);
                
                String consulta = "select TombamentoCodigo, TombamentoValorBem from Tombamento "
                        + "where TombamentoCodigo = "+ patrimonio +" and TombamentoTipo = 'bp';";
                banco.execute(consulta);
                while(banco.getResultSetConsulta().next())
                {
                    String valor_anterior = banco.getResultSetConsulta().getString("TombamentoValorBem");
                    System.out.println("|- Valores:");
                    System.out.println("|- Patrimonio - "+patrimonio);
                    System.out.println("|- Valor anterior do patrimonio - "+valor_anterior);
                    System.out.println("|- Valor atualizado do patrimonio - "+preco_atu_mod2);
                    System.out.println("|- Data de atualização - 17/11/2014");
                    
                    banco.executeUpdate("insert into TombamentoValoresHistorico (patrimonio, valor_anterior, valor_atualizado, data_atualizacao) values ("+ patrimonio +",'"+valor_anterior+"','"+ preco_atu_mod2 +"','17/11/2014');");
                    
                    System.out.println("|----------------------------------------------|");
                    System.out.println("");
                }
                
                linha = lerArq.readLine();
            }
        }
    }
}
